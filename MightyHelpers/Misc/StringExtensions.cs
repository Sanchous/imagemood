﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Text.RegularExpressions;

namespace MightyHelpers.Misc
{
    public static class StringExtensions
    {
        private static readonly Dictionary<char, string> TranslitDict;

        static StringExtensions()
        {
            //TODO - уточнить как правильно
            TranslitDict = new Dictionary<char, string>
                {
                    {'ж', "zh"},
                    {'ц', "ts"},
                    {'ч', "ch"},
                    {'ш', "sh"},
                    {'щ', "shch"},
                    {'ь', ""},
                    {'ю', "yu"},
                    {'я', "ya"}
                };
            const string rus = "абвгдеёзийклмнопрстуфхъыэ";
            const string eng = "abvgdeeziyklmnoprstufh'ie";

            for (int i = 0; i < rus.Length; i++)
            {
                TranslitDict.Add(rus[i], eng[i].ToString(CultureInfo.InvariantCulture));
            }
        }

        public static string Translit(this string source)
        {
            return source.ToFriendlyUrl();
            //return Transliterate(source.ToLowerInvariant());
            //if (string.IsNullOrEmpty(source))
            //{
            //    return string.Empty;
            //}

            //if (Regex.IsMatch(source, @"^[a-zA-Z0-9]"))
            //{
            //    return source.ToLowerInvariant().Replace("'", "").Replace(" ", "-");
            //}

            //var transliterator = new Transliterator(source);
            //return transliterator.Transliterate(spaceReplace).Replace("'", "").ToLowerInvariant();
        }

        private static string Transliterate(string source, int maxLen = 45)
        {
            source = Translate(source);

            source = Regex.Replace(source, @"[^a-z0-9\s-]", ""); // invalid chars           
            source = Regex.Replace(source, @"\s+", " ").Trim(); // convert multiple spaces into one space   
            source = source.Substring(0, source.Length <= maxLen ? source.Length : maxLen).Trim(); // cut and trim it   
            source = Regex.Replace(source, @"\s", "-"); // hyphens   

            return source;
        }

        private static string Translate(string input)
        {
            var sb = new StringBuilder();
            foreach (char ch in input)
            {
                if (TranslitDict.ContainsKey(ch))
                {
                    sb.Append(TranslitDict[ch]);
                }
                else
                {
                    sb.Append(ch);
                }
            }
            return sb.ToString();
        }

        private static string ToFriendlyUrl(this string source)
        {
            if (string.IsNullOrEmpty(source))
            {
                throw new ArgumentException("No source");
            }
            // if it's too long, clip it
            if (source.Length > 80)
                source = source.Substring(0, 79);
            // make it all lower case
            source = source.ToLower();

            source = Transliterate(source);
            // remove entities
            source = Regex.Replace(source, @"&\w+;", "");
            // remove anything that is not letters, numbers, dash, or space
            source = Regex.Replace(source, @"[^a-z0-9\-\s]", "");
            // replace spaces
            source = source.Replace(' ', '-');
            // collapse dashes
            source = Regex.Replace(source, @"-{2,}", "-");
            // trim excessive dashes at the beginning
            source = source.TrimStart(new[] {'-'});

            // remove trailing dashes
            source = source.TrimEnd(new[] {'-'});
            return source;
        }
    }
}