﻿using System.Linq.Expressions;

namespace MightyHelpers.LinqHelpers
{
    public static class Expressions
    {
        //public static Expression<Func<T, bool>> AndAlso<T>(this Expression<Func<T, bool>> left, Expression<Func<T, bool>> right)
        //{
        //    var param = Expression.Parameter(typeof (T), "x");
        //    var body = Expression.AndAlso(
        //        Expression.Invoke(left, param),
        //        Expression.Invoke(right, param)
        //        );
        //    var lambda = Expression.Lambda<Func<T, bool>>(body, param);
        //    return lambda;
        //}

        public static Expression<TDelegate> AndAlso<TDelegate>(this Expression<TDelegate> left,
                                                               Expression<TDelegate> right)
        {
            var body = Expression.AndAlso(
                left, right);

            var lambda = Expression.Lambda<TDelegate>(body);
            return lambda;
        }
    }
}