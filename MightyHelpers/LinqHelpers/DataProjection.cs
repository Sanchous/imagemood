﻿using System.Data.Entity;
using System.Linq;

namespace MightyHelpers.LinqHelpers
{
    public static class DataProjection
    {
        public static IQueryable<T> ReadOnly<T>(this IQueryable<T> source) where T : class
        {
            return source.AsNoTracking().AsQueryable();
        }
    }
}