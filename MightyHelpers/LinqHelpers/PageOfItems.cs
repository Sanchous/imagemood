﻿using System;
using System.Collections.Generic;

namespace MightyHelpers.LinqHelpers
{
    public class PageOfItems<T>
    {
        public PageOfItems(IEnumerable<T> items, int page, int pageSize, int itemsCount)
        {
            Items = items;
            Page = page;
            PageSize = pageSize;
            TotalPages = (int) Math.Floor((decimal) itemsCount/pageSize);
        }

        public IEnumerable<T> Items { get; private set; }
        public int Page { get; private set; }
        public int TotalPages { get; private set; }
        public int PageSize { get; private set; }

        public bool AnyMorePages
        {
            get { return TotalPages > Page; }
        }
    }
}