﻿using System;
using System.Linq;
using System.Linq.Expressions;
using EntityFramework.Extensions;

namespace MightyHelpers.LinqHelpers
{
    public static class OrderAndPaging
    {
        public static PageOfItems<T> ToPage<T>(this IQueryable<T> source, int page, int pageSize, int maxPageRange = 0) where T : class
        {
            if (!(source is IOrderedQueryable<T>) && page > 0)
            {
                throw new InvalidOperationException("Must be ordered");
            }

            var count = maxPageRange > 0
                            ? source.Take(page*(page + maxPageRange)).Count()
                            : source.Count();
                            
            if (page < 1 || page > CalculateTotalPages(count, pageSize))
            {
                throw new ArgumentException("Page out of range");
            }

            if (page > 1)
            {
                source = source.Skip((page + 1)*pageSize);
            }

            return new PageOfItems<T>(source.Take(pageSize).Future().ToArray(), page, pageSize, count);
        }

        public static int CalculateTotalPages(int count, int pageSize)
        {
            return (int) Math.Ceiling((decimal) count/pageSize);
        }

        public static IQueryable<T> TakePage<T>(this IOrderedQueryable<T> source, int page, int pageSize)
        {
            return source
                .Skip((page - 1)*pageSize)
                .Take(pageSize);
        }

        public static IOrderedQueryable<T> OrderBy<T>(this IQueryable<T> source, string property)
        {
            return ApplyOrder(source, property, "OrderBy");
        }

        public static IOrderedQueryable<T> ThenBy<T>(this IOrderedQueryable<T> source, string property)
        {
            return ApplyOrder(source, property, "ThenBy");
        }

        //private static IOrderedQueryable<T> ApplyOrder<T>(IQueryable<T> source, string property, string methodName)
        //{
        //    var props = property.Split('.');
        //    var type = typeof(T);
        //    var arg = Expression.Parameter(type, "x");
        //    Expression expr = arg;
        //    foreach (var prop in props)
        //    {
        //        // use reflection (not ComponentModel) to mirror LINQ
        //        var pi = type.GetProperty(prop);
        //        expr = Expression.Property(expr, pi);
        //        type = pi.PropertyType;
        //    }
        //    var delegateType = typeof(Func<,>).MakeGenericType(typeof(T), type);
        //    var lambda = Expression.Lambda(delegateType, expr, arg);

        //    var result = typeof(Queryable).GetMethods().Single(
        //        method => method.Name == methodName
        //                  && method.IsGenericMethodDefinition
        //                  && method.GetGenericArguments().Length == 2
        //                  && method.GetParameters().Length == 2)
        //        .MakeGenericMethod(typeof(T), type)
        //        .Invoke(null, new object[] { source, lambda });
        //    return (IOrderedQueryable<T>)result;
        //}

        private static IOrderedQueryable<T> ApplyOrder<T>(IQueryable<T> source, string orderByProperty,
                                                          string orderMethod)
        {
            if (source == null)
            {
                throw new ArgumentNullException("source");
            }

            int descIndex = orderByProperty.IndexOf(" desc", StringComparison.OrdinalIgnoreCase);
            if (descIndex != -1)
            {
                orderByProperty = orderByProperty.Substring(0, descIndex).Trim();
            }

            if (String.IsNullOrEmpty(orderByProperty))
            {
                return (IOrderedQueryable<T>) source;
            }

            ParameterExpression parameter = Expression.Parameter(source.ElementType);
            Expression property = orderByProperty.Split('.')
                                                 .Aggregate<string, Expression>(parameter, Expression.Property);

            LambdaExpression lambda = Expression.Lambda(property, parameter);

            orderMethod = descIndex == -1 ? orderMethod : orderMethod + "Descending";

            Expression methodCallExpression = Expression.Call(
                typeof (Queryable),
                orderMethod,
                new[]
                    {
                        source.ElementType,
                        property.Type
                    },
                source.Expression,
                Expression.Quote(lambda));

            return (IOrderedQueryable<T>) source.Provider.CreateQuery<T>(methodCallExpression);
        }
    }
}