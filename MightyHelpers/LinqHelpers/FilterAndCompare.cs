﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace MightyHelpers.LinqHelpers
{
    public static class FilterAndCompare
    {
        // Street magic below
        public static IQueryable<T> FilterBy<T>(this IQueryable<T> source, T comparerObject,
                                                IEnumerable<string> propertiesToCompare) where T : new()
        {
            var properties = comparerObject.GetType()
                                           .GetProperties()
                                           .Where(x => propertiesToCompare.Contains(x.Name));
            var defaultObjectValue = new T();

            foreach (var propertyInfo in properties)
            {
                var value = propertyInfo.GetValue(comparerObject, null);
                var defaultValue = propertyInfo.GetValue(defaultObjectValue, null);

                if (propertyInfo.PropertyType != typeof (Boolean) && ValuesAreEqual(value, defaultValue))
                    continue;

                if (propertyInfo.PropertyType == typeof (string))
                {
                    source = source.Where(ContainsPredicate<T>(propertyInfo.Name, value.ToString()));
                    continue;
                }

                var parameter = Expression.Parameter(typeof (T));
                var prop = Expression.Property(parameter, propertyInfo);

                var constant = Expression.Constant(value, propertyInfo.PropertyType);

                var condition = Expression.Equal(prop, constant);
                var lambda = Expression.Lambda<Func<T, bool>>(condition, parameter);
                source = source.Where(lambda);
            }

            return source;
        }

        public static bool AnySafe<T>(this IEnumerable<T> source)
        {
            return source != null && source.Any();
        }

        public static IQueryable<T> DistinctBy<T, TIdentity>(this IQueryable<T> source,
                                                             Func<T, TIdentity> identitySelector)
        {
            return source.Distinct(By(identitySelector));
        }

        private static bool ValuesAreEqual(object a, object b)
        {
            bool isEqual;

            var selfValueComparer = a as IComparable;

            if (a == null && b != null || a != null && b == null)
                isEqual = false; // one of the values is null
            else if (selfValueComparer != null && selfValueComparer.CompareTo(b) != 0)
                isEqual = false; // the comparison using IComparable failed
            else if (!Equals(a, b))
                isEqual = false; // the comparison using Equals failed
            else
                isEqual = true; // match

            return isEqual;
        }

        private static Expression<Func<T, bool>> ContainsPredicate<T>(string propertyName,
                                                                      string searchValue)
        {
            ParameterExpression parameter = Expression.Parameter(typeof (T), "x");
            Expression property = Expression.Property(parameter, propertyName);
            //Expression toStringCall = Expression.Call(
            //    property, "ToString", null
            //    );

            Expression containsCall = Expression.Call(
                property, "Contains",
                null,
                new Expression[] {Expression.Constant(searchValue)});

            return Expression.Lambda<Func<T, bool>>(containsCall, parameter);
        }


        private static IEqualityComparer<TSource> By<TSource, TIdentity>(Func<TSource, TIdentity> identitySelector)
        {
            return new DelegateComparer<TSource, TIdentity>(identitySelector);
        }

        private class DelegateComparer<T, TIdentity> : IEqualityComparer<T>
        {
            private readonly Func<T, TIdentity> _identitySelector;

            public DelegateComparer(Func<T, TIdentity> identitySelector)
            {
                _identitySelector = identitySelector;
            }

            public bool Equals(T x, T y)
            {
                return Equals(_identitySelector(x), _identitySelector(y));
            }

            public int GetHashCode(T obj)
            {
                return _identitySelector(obj).GetHashCode();
            }
        }
    }
}