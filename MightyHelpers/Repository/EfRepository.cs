﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using MightyHelpers.LinqHelpers;

namespace MightyHelpers.Repository
{
    public class EfRepository<T> : IRepository<T> where T : Entity, IAggregateRoot, new()
    {
        protected readonly DbContext Db;

        protected EfRepository(DbContext db)
        {
            Db = db;
        }

        public IQueryable<T> All { get { return Db.Set<T>(); } }


        public IQueryable<T> AllOf<TProperty>(Expression<Func<T, bool>> predicate, params Expression<Func<T, TProperty>>[] includeProperties)
        {
            var set = Db.Set<T>().AsQueryable();
            set = includeProperties.Aggregate(set, (current, prop) => current.Include(prop));
            set = set.Where(predicate);

            return set;
        }

        public PageOfItems<T> Page<TProperty>(Expression<Func<T, bool>> predicate, int page, int pageSize, params Expression<Func<T, TProperty>>[] includeProperties)
        {
            var set = Db.Set<T>().AsQueryable();
            set = includeProperties.Aggregate(set, (current, prop) => current.Include(prop));
            set = set.Where(predicate);

            return set.ToPage(page, pageSize);
        }

        public T Find(int id, params Expression<Func<T, object>>[] includeProperties)
        {
            var set = Db.Set<T>().AsQueryable();
            set = includeProperties.Aggregate(set, (current, prop) => current.Include(prop));
            return set.SingleOrDefault(x => x.Id == id);
        }

        public T Find(Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] includeProperties)
        {
            var set = Db.Set<T>().AsQueryable();
            set = includeProperties.Aggregate(set, (current, prop) => current.Include(prop));
            return set.SingleOrDefault(predicate);
        }

        public void LoadReference<TProperty>(T entity, Expression<Func<T, TProperty>> reference) where TProperty : class
        {
            Db.Entry(entity).Reference(reference).Load();
        }

        public void LoadCollection<TElem>(T entity, Expression<Func<T, ICollection<TElem>>> collection)
            where TElem : class
        {
            Db.Entry(entity).Collection(collection).Load();
        }

        public void EnsureAttached(T entity)
        {
            if (Db.Entry(entity).State == EntityState.Detached)
                throw new InvalidOperationException("Llama hands: Entity not attached");
        }

        public T Add(T entity)
        {
            Db.Set<T>().Add(entity);
            return entity;
        }

        public T Update(T entity, IEnumerable<string> properties)
        {
            Db.Set<T>().Attach(entity);
            var entry = Db.Entry(entity);
            foreach (var property in properties)
            {
                entry.Property(property).IsModified = true;
            }

            return entity;
        }

        public T Update<TProperty>(T entity, params Expression<Func<T, TProperty>>[] properties)
        {
            Db.Set<T>().Attach(entity);
            var entry = Db.Entry(entity);
            foreach (var property in properties)
            {
                entry.Property(property).IsModified = true;
            }

            return entity;
        }

        public void Remove(int id)
        {

            var entity = Db.Set<T>().Find(id) ?? new T {Id = id};
            if (Db.Entry(entity).State == EntityState.Detached)
            {
                Db.Set<T>().Attach(entity);
            }
            Db.Set<T>().Remove(entity);

        }

        public void Remove(T entity)
        {
            Db.Set<T>().Remove(entity);
        }

        public void SaveChanges()
        {
            Db.SaveChanges();
        }
    }
}