﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using MightyHelpers.Models;

namespace MightyHelpers.Repository
{
    public abstract class Entity
    {
        [Key]
        public int Id { get; internal set; }

        // Все ошибки при любой проверке хранятся здесь.
        // При ForceRulesValidation выбрасывается эксепшн, при сохранении - обычное поведение IValidatable
        private readonly RuleException _ruleException = new RuleException();

        public void AddRuleException(string fieldName, string error)
        {
            _ruleException.AddError(fieldName, error);
        }

        // Получить дополнительные правила валидации для объекта
        void GetValidatorErrors()
        {

            foreach (var rule in EntityRules())
            {
                foreach (var fieldName in rule.MemberNames)
                {
                    _ruleException.AddError(fieldName, rule.ErrorMessage);
                }
            }
        }
        // Принудительная проверка правил до сохранения
        public void ForceRulesValidation()
        {
            GetValidatorErrors();
            _ruleException.Check();
        }

        protected virtual IEnumerable<ValidationResult> EntityRules()
        {
            return new List<ValidationResult>();
        }


        public virtual IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            GetValidatorErrors();

            foreach (string key in _ruleException.Errors)
            {
                var values = _ruleException.Errors.GetValues(key);
                if (values != null)
                    foreach (var value in values)
                    {
                        yield return new ValidationResult(key, new[] { value });
                    }
            }
        }
    }
}