﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using MightyHelpers.LinqHelpers;

namespace MightyHelpers.Repository
{
    public interface IRepository<T> where T : Entity, IAggregateRoot
    {
        IQueryable<T> All { get; }

    IQueryable<T> AllOf<TProperty>(Expression<Func<T, bool>> predicate,
                                        params Expression<Func<T, TProperty>>[] includeProperties);

        PageOfItems<T> Page<TProperty>(Expression<Func<T, bool>> predicate,
                                         int page, int pageSize, params Expression<Func<T, TProperty>>[] includeProperties);

        T Find(int id, params Expression<Func<T, object>>[] includeProperties);

        T Find(Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] includeProperties);

        void LoadReference<TProperty>(T entity, Expression<Func<T, TProperty>> reference) where TProperty : class;

        void LoadCollection<TElem>(T entity, Expression<Func<T, ICollection<TElem>>> collection) where TElem : class;

        void EnsureAttached(T entity);

        T Add(T entity);
        T Update(T entity, IEnumerable<string> properties);
        T Update<TProperty>(T entity, params Expression<Func<T, TProperty>>[] properties);
        void Remove(int id);
        void Remove(T entity);

        void SaveChanges();
    }
}