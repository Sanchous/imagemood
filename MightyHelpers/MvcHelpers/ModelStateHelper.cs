﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace MightyHelpers.MvcHelpers
{
    public static class ModelStateHelper
    {
        public static IEnumerable<string> FormPropertiesOf<T>(this ModelStateDictionary modelState, T sourceObject)
        {
            var properties = sourceObject.GetType().GetProperties().Select(s => s.Name);
            return modelState.Keys.Intersect(properties).ToList();
        }
    }
}