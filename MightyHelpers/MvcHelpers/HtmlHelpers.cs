﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.WebPages;

namespace MightyHelpers.MvcHelpers
{
    public static class HtmlHelpers
    {
        public static HtmlHelper GetPageHelper(this System.Web.WebPages.Html.HtmlHelper html)
        {
            return ((WebViewPage) WebPageContext.Current.Page).Html;
        }

        public static AjaxHelper GetAjaxPageHelper(this System.Web.WebPages.Html.HtmlHelper html)
        {
            return ((WebViewPage) WebPageContext.Current.Page).Ajax;
        }

        private static string MakeLinks(this string txt)
        {
            var regx =
                new Regex(
                    "http(s)?://([\\w+?\\.\\w+])+([a-zA-Z0-9\\~\\!\\@\\#\\$\\%\\^\\&amp;\\*\\(\\)_\\-\\=\\+\\\\\\/\\?\\.\\:\\;\\'\\,]*)?",
                    RegexOptions.IgnoreCase);

            MatchCollection mactches = regx.Matches(txt);

            foreach (Match match in mactches)
            {
                //TODO: correct filtering AntiXSS
                var value = match.Value.Replace("mailto:", "#");
                value = value.Replace("javascript:", "#");
                txt = txt.Replace(value, "<a href='" + match.Value + "' target='_blank'>" + match.Value + "</a>");
            }

            return txt;
        }

        public static MvcHtmlString MultiLine(this string source, bool makeLinks = true)
        {
            if (String.IsNullOrWhiteSpace(source))
                return new MvcHtmlString(String.Empty);

            var res = String.Join(
                "<br />",
                source
                    .Split(new[] {Environment.NewLine}, StringSplitOptions.None)
                    .Select(HttpUtility.HtmlEncode)
                );

            return new MvcHtmlString(makeLinks ? res.MakeLinks() : res);
        }

        public static SelectList ToSelectList<TEnum>(this TEnum enumObj, bool idAsInt = false, bool selected = true)
        {
            var values = from TEnum e in Enum.GetValues(typeof (TEnum))
                         select new {Id = idAsInt ? Convert.ToInt32(e).ToString(CultureInfo.InvariantCulture) : e.ToString(), Name = e.ToString()};

            return new SelectList(values, "Id", "Name",
                                  selected
                                      ? idAsInt
                                            ? Convert.ToInt32(enumObj).ToString(CultureInfo.InvariantCulture)
                                            : enumObj.ToString()
                                      : null
                );
        }

        public static bool IsCurrentAction(this HtmlHelper helper, string actionName, string controllerName)
        {
            var currentControllerName = (string) helper.ViewContext.RouteData.Values["controller"];
            var currentActionName = (string) helper.ViewContext.RouteData.Values["action"];

            return currentControllerName.Equals(controllerName, StringComparison.CurrentCultureIgnoreCase)
                   && (actionName.Equals("any", StringComparison.CurrentCultureIgnoreCase)
                       || currentActionName.Equals(actionName, StringComparison.CurrentCultureIgnoreCase));
        }

        public static string Preview(this string source, int maxChars, bool toNearSpace = false)
        {
            if (String.IsNullOrEmpty(source) || source.Length < maxChars)
                return source;

            var substring = source.Substring(0, maxChars);
            if (toNearSpace)
            {
                substring = substring.Substring(0, substring.LastIndexOf(' '));
            }
            return substring + "...";
        }
        // Usage: @using(Html.BeginScripts()) {your inline or referensed scripts} anywhere in views
        // @Html.RenderPageScripts - before </body> tag or inside <head> tag
        private class ScriptBlock : IDisposable
        {
            private const string ScriptsKey = "scripts";

            public static List<string> PageScripts
            {
                get
                {
                    if (HttpContext.Current.Items[ScriptsKey] == null)
                        HttpContext.Current.Items[ScriptsKey] = new List<string>();
                    return (List<string>) HttpContext.Current.Items[ScriptsKey];
                }
            }

            private readonly WebViewPage _webPageBase;

            public ScriptBlock(WebViewPage webPageBase)
            {
                _webPageBase = webPageBase;
                _webPageBase.OutputStack.Push(new StringWriter());
            }

            public void Dispose()
            {
                PageScripts.Add(_webPageBase.OutputStack.Pop().ToString());
            }
        }

        public static IDisposable BeginScripts(this HtmlHelper helper)
        {
            if (helper.ViewContext.RequestContext.HttpContext.Request.IsAjaxRequest())
                return null;
            return new ScriptBlock((WebViewPage) helper.ViewDataContainer);
        }


        public static MvcHtmlString RenderPageScripts(this HtmlHelper helper)
        {
            return
                MvcHtmlString.Create(String.Join(Environment.NewLine,
                                                 ScriptBlock.PageScripts.Select(
                                                     s => s.ToString(CultureInfo.InvariantCulture))));
        }
    }
}