﻿using System;
using System.Collections.Specialized;
using System.Web.Mvc;

namespace MightyHelpers.Models
{
	[Serializable]
    public class RuleException : Exception
    {
        private readonly NameValueCollection _errors;

        public RuleException()
        {
            _errors = new NameValueCollection();
        }

        public void Check()
        {
            if (_errors.Count > 0)
            {
                throw this;
            }
        }
        public NameValueCollection Errors { get { return _errors; } }
        public void AddError(string fieldName, string error)
        {
            _errors.Add(fieldName, error);
        }

        public RuleException(string fieldName, string error)
        {
            _errors = new NameValueCollection { { fieldName, error } };
        }
        public RuleException (NameValueCollection errors)
        {
            _errors = errors;
        }
        public void CopyErrorsToModelState(ModelStateDictionary modelState, string prefix = null)
        {
            foreach (string key in _errors)
            {
                var errValues = _errors.GetValues(key);
                if (errValues != null)
                    foreach (var value in errValues)
                    {
                        if (string.IsNullOrEmpty(prefix))
                        {
                            modelState.AddModelError(prefix + "." + key, value);
                        }
                        else
                        {
                            modelState.AddModelError(key, value);
                        }                            
                    }
            }
        }
    }
}