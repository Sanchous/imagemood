﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;
using MightyHelpers.Repository;

namespace ImageMood.Entities
{
    public class Mood : Entity, IAggregateRoot
    {
        public Mood()
        {
            MoodToImages = new Collection<MoodToImage>();
        }


        [Required(ErrorMessage = "Come on, what is your mood?")]
        [StringLength(64)]
        [RegularExpression(@"([A-яa-я ])+", ErrorMessage = "Try to express your mood with words")]
        [Remote("CheckMoodName", "Moods", ErrorMessage = "This mood already exists", AdditionalFields = "ImageId")]
        public string Name { get; set; }

        [NotMapped]
        public int ImageId { get; set; }

        public int Weight { get; private set; }

        public int ImagesCount { get; private set; }

        public ICollection<MoodToImage> MoodToImages { get; set; }

        public DateTime Created { get; private set; }


        public void AddWeight(int value)
        {
            Weight += value;
        }

        public void Create()
        {
            Created = DateTime.Now;
            ImagesCount = 1;
            Weight = 1;
        }

        protected override IEnumerable<ValidationResult> EntityRules()
        {
            return new List<ValidationResult>();
        }
    }
}