﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using MightyHelpers.Repository;


namespace ImageMood.Entities
{
    public class ImageMoodVote : Entity
    {

        public MoodToImage MoodToImage { get; set; }
        
        public Guid AnonymousGuid { get; set; }

        public bool Negative { get; set; }


        protected override IEnumerable<ValidationResult> EntityRules()
        {
            return new List<ValidationResult>();
        }
    }
}