﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Script.Serialization;
using ImageMood.Helpers;
using ImageMood.Models;
using ImageResizer;
using ImageResizer.Plugins.RemoteReader;
using MightyHelpers.Repository;

namespace ImageMood.Entities
{
    public class Image : Entity, IAggregateRoot
    {
        public Image()
        {
            ImageMoods = new Collection<MoodToImage>();
        }

        [Required(ErrorMessage = "Upload image file first")]
        [StringLength(255, ErrorMessage = "Too long {0}")]
        [DataType(DataType.ImageUrl)]
        public string Url { get; set; }

        [StringLength(32)]
        [ScriptIgnore]
        public string DeleteHash { get; set; }

        [System.ComponentModel.DataAnnotations.Schema.NotMapped]
        public string Thumb
        {
            get { return Url.ImgurThumb(ImgurThumbType.LargeThumbnail); }
        }

        public string FullUrl
        {
            get
            {
                return MaxWidth > 0
                           ? RemoteReaderPlugin.Current.CreateSignedUrl(Url, new ResizeSettings(string.Concat(
                               "width=", MaxWidth > 0)))
                           : Url;
            }
        }

        [NotMapped]
        public int MaxWidth { internal get; set; }

        [NotMapped]
        [Required(ErrorMessage = "Chose all the feels you feel")]
        [RegularExpression(@"([A-яa-я ,])+", ErrorMessage = "Try to express your moods with words (use commas, Luke)")]
        public string MoodsString { get; set; }

        [StringLength(128, ErrorMessage = "Too long {0}")]
        public string Name { get; set; }

        [StringLength(255, ErrorMessage = "Too long {0}")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        [StringLength(64)]
        [ScriptIgnore]
        public string Ip { get; private set; }

        public DateTime Created { get; private set; }

        public ICollection<MoodToImage> ImageMoods { get; private set; }
        public ICollection<ReportCp> Reports { get; set; }


        public void Create(UserContext userContext, string ip)
        {
            Created = DateTime.Now;
            Ip = ip;
        }

        public void AddMood(Mood mood, UserContext userContext)
        {
            if (mood.Id == 0)
            {
                mood.Create();
            }
            var moodToImage = new MoodToImage {Mood = mood};
            moodToImage.Vote(userContext.UserGuid);
            ImageMoods.Add(moodToImage);
        }

        protected override IEnumerable<ValidationResult> EntityRules()
        {
            return new List<ValidationResult>();
        }
    }
}