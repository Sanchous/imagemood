﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Script.Serialization;
using MightyHelpers.Repository;

namespace ImageMood.Entities
{
    public class MoodToImage : Entity, IAggregateRoot
    {
        public MoodToImage()
        {
            Votes = new Collection<ImageMoodVote>();
        }

        public int MoodId { get; set; }

        public int ImageId { get; set; }

        public Image Image { get; set; }

        [ScriptIgnore]
        public Mood Mood { get; set; }

        public int Rating { get; private set; }

        public ICollection<ImageMoodVote> Votes { get; private set; }

        public void Vote(Guid anonymousGuid, bool plus = true)
        {
            if (plus)
            {
                Votes.Add(new ImageMoodVote {AnonymousGuid = anonymousGuid});
            }
            else
            {
                Votes.Remove(Votes.Single(x => x.AnonymousGuid == anonymousGuid));
            }
            var ratingShift = plus ? 1 : -1;
            Mood.AddWeight(ratingShift);
            Rating += ratingShift;
        }

        protected override IEnumerable<ValidationResult> EntityRules()
        {
            return new List<ValidationResult>();
        }
    }
}