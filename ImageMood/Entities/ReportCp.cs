﻿using System.ComponentModel.DataAnnotations;

namespace ImageMood.Entities
{
    public class ReportCp
    {
        public int Id { get; private set; }

        public int ImageId { get; set; }
        public Image Image { get; set; }

        [Required]
        [StringLength(512, ErrorMessage = "Max length 512")]
        public string Text { get; set; }
    }
}