﻿using System.Collections.Generic;
using ImageMood.Entities;
using ImageMood.ViewModels;

namespace ImageMood.Services
{
    public interface IImageService
    {
        IEnumerable<Image> GetImages();

        Image AddImage(Image image, string ip);

        void UploadToImgur(Image image);

        ImageViewModel GetImage(int id, int imageId);

        void ReportCp(ReportCp report);
        IEnumerable<RandomImageModel> GetRandomImages(int imagesCount);
        void DeleteImage(int id);
    }
}