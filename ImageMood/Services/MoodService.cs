﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Entity;
using System.Linq;
using ImageMood.Data;
using ImageMood.Entities;
using ImageMood.Enums;
using ImageMood.Models;
using ImageMood.ViewModels;
using MightyHelpers.LinqHelpers;
namespace ImageMood.Services
{
    public class MoodService : BaseService, IMoodsService
    {
        private readonly IMoodRepository _moodRepository;
        private readonly IImageRepository _imageRepository;

        public MoodService(IMoodRepository moodRepository, UserContext userContext, IImageRepository imageRepository)
            : base(userContext)
        {
            _moodRepository = moodRepository;
            _imageRepository = imageRepository;
        }

        public ImageMoodViewModel AddMood(Mood mood, int imageId)
        {
            if (mood == null) throw new ArgumentNullException("mood");

            if (IsMoodExists(mood.Name, imageId))
            {
                throw new ArgumentException("Mood already exist for image");
            }

            var existingMood = _moodRepository.Find(x => x.Name.ToUpper() == mood.Name.ToUpper(), i => i.MoodToImages);
            var moodToAdd = existingMood ?? mood;

            var image = _imageRepository.Find(imageId);

            image.AddMood(moodToAdd, UserContext);

            _imageRepository.SaveChanges();

            var moodToImage = moodToAdd.MoodToImages.First(x => x.ImageId == imageId);

            return new ImageMoodViewModel
                {
                    Id = moodToImage.Id,
                    Mood = new MoodViewModel
                        {
                            Id = moodToImage.Mood.Id,
                            Name = moodToImage.Mood.Name
                        },
                        Rating = moodToImage.Rating,
                        Voted = true
                };
        }

        public MoodToImage VoteMood(int id, bool plus)
        {
            if (UserContext.VotedIds.Contains(id) && plus)
            {
                throw new AccessViolationException("Already voted");
            }
            var model = _moodRepository.All
                                       .SelectMany(s => s.MoodToImages)
                                       .Include(i => i.Mood)
                                       .Include(i => i.Votes)
                                       .SingleOrDefault(x => x.Id == id
                                                             &&
                                                             (plus
                                                                  ? x.Votes.All(
                                                                      a => a.AnonymousGuid != UserContext.UserGuid)
                                                                  : x.Votes.Any(
                                                                      a => a.AnonymousGuid == UserContext.UserGuid)));

            if (model == null)
            {
                if (_moodRepository.All.Any(a => a.MoodToImages.Any(m => m.Id == id)))
                {
                    throw new AccessViolationException("Already voted");
                }
                throw new NotFoundException("Vote for mood");
            }

            model.Vote(UserContext.UserGuid, plus);

            _moodRepository.SaveChanges();

            if (plus)
            {
                UserContext.VotedIds.Add(id);
            }
            else
            {
                UserContext.VotedIds.Remove(id);
            }

            return model;
        }

        public IEnumerable<Image> FindImagesForMood(int id, ImageOrder imageOrder, int page, int pageSize)
        {
            var query = _moodRepository.All
                                       .Where(x => x.Id == id)
                                       .SelectMany(s => s.MoodToImages)
                                       ;
            IOrderedQueryable<Image> ordered;

            switch (imageOrder)
            {
                    case ImageOrder.New:
                    ordered = query.Select(s => s.Image).Distinct().OrderByDescending(o => o.Created);
                    break;

                    case ImageOrder.Relevant:
                    ordered = query.Distinct().OrderByDescending(o => o.Rating).Select(s => s.Image).Distinct().OrderBy(o => 0);
                    break;

                    case ImageOrder.Shuffle:
                    ordered = query.Select(s => s.Image).Distinct().OrderBy(o => Guid.NewGuid());
                    break;

                    default:
                    throw new InvalidEnumArgumentException("Invalid enum value for ImageOrder");
                    
            }

            return ordered
                .TakePage(page, pageSize)
                .ToList();

        }

        public IEnumerable<Mood> GetMoods(string moodName, MoodOrder moodOrder, int page, int pageSize)
        {
            var query = _moodRepository.All;
            if (!string.IsNullOrEmpty(moodName))
            {
                query = query.Where(x => x.Name.Contains(moodName));
            }
            IOrderedQueryable<Mood> ordered;

            switch (moodOrder)
            {
                case MoodOrder.Rating:
                    ordered = query.OrderByDescending(o => o.Weight);
                    break;
                case MoodOrder.New:
                    ordered = query.OrderByDescending(o => o.Created);
                    break;
                case MoodOrder.Popular:
                    ordered = query.OrderByDescending(o => o.MoodToImages.Count);
                    break;
                case MoodOrder.Random:
                    ordered = query.OrderBy(o => Guid.NewGuid());
                    page = 1;
                    break;
                default:
                    throw new InvalidEnumArgumentException("Invalid enum value for MoodOrder");
            }

            return ordered
                .TakePage(page, pageSize)
                .ToList();
        }

        public bool IsMoodExists(string name, int? imageId)
        {
            name = name.Trim().ToUpper();
            return _moodRepository.All
                                  .Any(a => a.Name.ToUpper() == name
                                            &&
                                            (!imageId.HasValue || imageId == 0 || a.MoodToImages.Any(m => m.ImageId == imageId.Value)));
        }


    }
}