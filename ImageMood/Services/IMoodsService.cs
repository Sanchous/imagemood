﻿using System.Collections.Generic;
using ImageMood.Entities;
using ImageMood.Enums;
using ImageMood.ViewModels;

namespace ImageMood.Services
{
    public interface IMoodsService
    {
        ImageMoodViewModel AddMood(Mood mood, int imageId);

        MoodToImage VoteMood(int id, bool plus);
        IEnumerable<Image> FindImagesForMood(int id, ImageOrder imageOrder, int page, int pageSize);
        IEnumerable<Mood> GetMoods(string moodName, MoodOrder moodOrder, int page, int pageSize);
        bool IsMoodExists(string name, int? imageId);
        
    }
}