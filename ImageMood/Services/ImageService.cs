﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Xml.Linq;
using ImageMood.Data;
using ImageMood.Entities;
using ImageMood.Models;
using ImageMood.ViewModels;
using MightyHelpers.LinqHelpers;
using System.Configuration;

namespace ImageMood.Services
{
    public class ImageService : BaseService, IImageService
    {
        private readonly IImageRepository _imageRepository;
        private readonly IMoodRepository _moodRepository;
        private readonly string ImgurClientId = ConfigurationManager.AppSettings["ImgurClientID"];
        public ImageService(UserContext userContext, IImageRepository imageRepository, IMoodRepository moodRepository) : base(userContext)
        {
            _imageRepository = imageRepository;
            _moodRepository = moodRepository;
        }

        public IEnumerable<Image> GetImages()
        {
            var imagesToUpload = _imageRepository.All
                                                 .Where(x => !x.Url.Contains("imgur.com"))
                                                 .ToList();

            foreach (var image in imagesToUpload)
            {
                image.MoodsString = "foo";
                UploadToImgur(image);
                _imageRepository.SaveChanges();
            }
            
            
            return _imageRepository.All
                                   .OrderByDescending(o => o.Created)
                                   .ToArray();
        }

        public Image AddImage(Image image, string ip)
        {
            if (string.IsNullOrEmpty(ip))
            {
                throw new ArgumentException("No ip");
            }
            var moods = image.MoodsString
                .Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries)
                .Where(x => !string.IsNullOrWhiteSpace(x))
                .Select(s => s.Trim())
                .ToArray();

            if (!moods.AnySafe())
            {
                throw new ArgumentException("No moods");
            }            
            var existingMoods = _moodRepository.All
                .Where(x => moods.Any(a => a.ToUpper() == x.Name.ToUpper()))
                .ToList();

            var newMoods = moods.Where(x => existingMoods.All(a => a.Name.ToUpper() != x.ToUpper())).Select(s => new Mood {Name = s});


            existingMoods.AddRange(newMoods);

            image.Create(UserContext, ip);

            foreach (var mood in existingMoods)
            {
                image.AddMood(mood, UserContext);
            }

            UploadToImgur(image);

            _imageRepository.Add(image);

            _imageRepository.SaveChanges();

            return image;
        }

        public void UploadToImgur(Image image)
        {

            if (image.Url.Contains("imgur.com"))
            {
                throw new InvalidOperationException("Already on Imgur");
            }


                var uploadRequestString =
                HttpUtility.UrlEncode("image", Encoding.UTF8) +
                "=" +
                HttpUtility.UrlEncode(image.Url);

                var webRequest = (HttpWebRequest)WebRequest.Create("https://api.imgur.com/3/upload.xml");
                webRequest.Method = WebRequestMethods.Http.Post;
                webRequest.ContentType = "application/x-www-form-urlencoded";
                webRequest.ServicePoint.Expect100Continue = false;
                webRequest.KeepAlive = true;
                webRequest.Headers.Add(HttpRequestHeader.Authorization, ImgurClientId);

                var streamWriter = new StreamWriter(webRequest.GetRequestStream());

                streamWriter.Write(uploadRequestString);
                streamWriter.Close();
                var response = webRequest.GetResponse();
                var responseStream = response.GetResponseStream();

                var result = XDocument.Load(responseStream);
                var link = result.Root.Descendants().Single(x => x.Name == "link").Value;

                var deleteHash = result.Root.Descendants().Single(x => x.Name == "deletehash").Value;

                image.Url = link;
                image.DeleteHash = deleteHash;

        }

        public ImageViewModel GetImage(int id, int imageId)
        {
            var model = _imageRepository.All
                .Where(x => x.Id == imageId && x.ImageMoods.Any(a => a.MoodId == id))
                .Select(ImageViewModel.Selector(UserContext.UserGuid))
                .SingleOrDefault();

            if (model == null)
            {
                throw new NotFoundException("Image not found");
            }
            return model;

        }

        public void ReportCp(ReportCp report)
        {
            var image = _imageRepository.Find(report.Id, i => i.Reports);
            
            image.Reports.Add(report);
            _imageRepository.SaveChanges();

        }

        public IEnumerable<RandomImageModel> GetRandomImages(int imagesCount)
        {
            var query = _imageRepository.All
                                        .OrderBy(o => Guid.NewGuid())
                                        .Select(RandomImageModel.Selector)
                                        .Take(imagesCount)
                                        .ToList();

            return query;

        }

        public void DeleteImage(int id)
        {
            var image = _imageRepository.Find(id);

            

            if (!string.IsNullOrWhiteSpace(image.DeleteHash))
            {
                var deleteEndpoint = string.Format("https://api.imgur.com/3/image/{0}.xml", image.DeleteHash);

                var webRequest = (HttpWebRequest) WebRequest.Create(deleteEndpoint);
                webRequest.Method = "DELETE";
                webRequest.ServicePoint.Expect100Continue = false;
                webRequest.Headers.Add(HttpRequestHeader.Authorization, ImgurClientId);

                var responseStream = webRequest.GetResponse().GetResponseStream();
                var result = XDocument.Load(responseStream);
                var success = result.Root.Attributes().Single(x => x.Name == "success").Value == "1";
                if (!success)
                {
                    throw new Exception("No success with Imgur :(");
                }
            }
            var emptyMoods = _moodRepository.All
                                            .Where(x => x.MoodToImages.All(a => a.ImageId == id));
            foreach (var emptyMood in emptyMoods)
            {
                _moodRepository.Remove(emptyMood);
            }
            

            _imageRepository.Remove(image);
            _imageRepository.SaveChanges();            
        }
    }
}