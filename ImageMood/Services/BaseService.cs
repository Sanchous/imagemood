﻿using ImageMood.Models;

namespace ImageMood.Services
{
    public abstract class BaseService
    {
        protected readonly UserContext UserContext;

        protected BaseService(UserContext userContext)
        {

            UserContext = userContext;
        }

    }
}