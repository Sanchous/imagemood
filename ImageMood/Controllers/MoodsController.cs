﻿using System.Linq;
using System.Web.Mvc;
using System.Web.UI;
using ImageMood.Entities;
using ImageMood.Enums;
using ImageMood.Filters;
using ImageMood.Services;

namespace ImageMood.Controllers
{
    [JsonForAjaxCalls]
    public partial class MoodsController : Controller
    {
        private readonly IMoodsService _moodsService;
        public const int MoodsPageSize = 100;
        public const int ImagesPageSize = 100;
        //
        // GET: /Mood/
        public MoodsController(IMoodsService moodsService)
        {
            _moodsService = moodsService;
        }
        [WarnAboutIe6]
        public virtual ActionResult Index()
        {
            return View();
        }

        public virtual ActionResult GetMoods(string moodName, MoodOrder moodOrder = MoodOrder.Rating, int page = 1)
        {            
            var model = _moodsService.GetMoods(moodName, moodOrder, page, MoodsPageSize);

            return Json(model, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public virtual ActionResult AddMood(Mood mood, int imageId)
        {
            var imageMoodViewModel = _moodsService.AddMood(mood, imageId);
            return View(imageMoodViewModel);
        }

        [HttpPost]
        public virtual ActionResult Vote(int id, bool plus = true)
        {
            var model = _moodsService.VoteMood(id, plus);
            return Json(new { model.Rating }, JsonRequestBehavior.AllowGet);
        }
        public virtual ActionResult FindImages(int id, ImageOrder imageOrder = ImageOrder.Shuffle, int page = 1)
        {
            var model = _moodsService.FindImagesForMood(id, imageOrder, page, ImagesPageSize);
            return View(model);
        }

        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        public virtual ActionResult CheckMoodName(string name, int? imageId)
        {
            var result = !_moodsService.IsMoodExists(name, imageId);
            return View(result);
        }

        public virtual ActionResult QuickSearch(string term)
        {
            var model = _moodsService.GetMoods(term, MoodOrder.Popular, 1, 100)
                .Select(s => new { s.Name, s.Id, label = s.Name, value = s.Name });

            return View(model);
        }
    }
}