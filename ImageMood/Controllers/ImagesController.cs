﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using ImageMood.Entities;
using ImageMood.Filters;
using ImageMood.Services;

namespace ImageMood.Controllers
{
    [JsonForAjaxCalls]
    public class ImagesController : AsyncController
    {
        private const int MaxDimension = 1024;
        private readonly IImageService _imageService;

        public ImagesController(IImageService imageService)
        {
            _imageService = imageService;
        }

        [Authorize]
        public virtual ActionResult List()
        {
            var model = _imageService.GetImages();
            
            return  View(model);
        }

        [Authorize]
        [HttpPost]
        public virtual ActionResult Delete(int id)
        {
            _imageService.DeleteImage(id);
            return Json(new {success = true}, JsonRequestBehavior.AllowGet);
        }

        public virtual ActionResult GetImage(int id, int imageId, int width = 0)
        {
            var model = _imageService.GetImage(id, imageId);
            model.Image.MaxWidth = width;
            return View(model); 
        }  
      
        public virtual ActionResult GetRandomImage()
        {
            var model = _imageService.GetRandomImages(5);
            return View(model);
        }

        [HttpPost]
        public virtual ActionResult Add(Image image)
        {
            //TODO - check Imgur uploading 
            if (!ModelState.IsValid)
            {
                return Json(new {error = "Model state invalid"});
            }

            var model = _imageService.AddImage(image, Request.ServerVariables["REMOTE_ADDR"]);



            return
                Json(
                    new
                        {
                            model.Name,
                            model.Id,
                            model.ImageMoods.First().MoodId,
                            MoodName = model.ImageMoods.First().Mood.Name,
                        });
        }


        [HttpPost]
        public virtual ActionResult Upload(HttpPostedFileBase image, string imageUrl)
        {

            try
            {

                var i = new ImageResizer.ImageJob(image ?? (object) imageUrl, "~/Content/Temp/<guid>.<ext>",
                                                  new ImageResizer.ResizeSettings(
                                                      string.Format("width={0};height={0};mode=max",
                                                                    MaxDimension))) {AllowDestinationPathVariables = true};
                i.Build();
                
                var sitePath = Server.MapPath(@"~");
                var relativePath = i.FinalPath.Replace(sitePath, @"~/");

                var url = ResolveServerUrl(VirtualPathUtility.ToAbsolute(relativePath), false);
            //    System.IO.File.Delete(i.FinalPath);

                return Json(new { success = true, imageUrl = url }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, error = ex.Message }, "text/html", JsonRequestBehavior.AllowGet);
            }
        }

        private static string ResolveServerUrl(string serverUrl, bool forceHttps)
        {
            if (serverUrl.IndexOf("://", StringComparison.Ordinal) > -1)
                return serverUrl;

            string newUrl = serverUrl;
            Uri originalUri = System.Web.HttpContext.Current.Request.Url;
            newUrl = (forceHttps ? "https" : originalUri.Scheme) +
                     "://" + originalUri.Authority + newUrl;
            return newUrl;
        } 

        [HttpPost]
        public virtual ActionResult ReportCp(ReportCp reportCp)
        {
            _imageService.ReportCp(reportCp);
            return Json(new {success = true}, JsonRequestBehavior.AllowGet);
        }
        
    }
}
