﻿using System.ComponentModel;
using ImageResizer;
using ImageResizer.Plugins.RemoteReader;

namespace ImageMood.Helpers
{
    public static class ImgurHelper
    {
        public static string ImgurThumb(this string source, ImgurThumbType thumbType = ImgurThumbType.SmallSquare)
        {
            if (string.IsNullOrEmpty(source))
            {
                return string.Empty;
            }
            if (!source.Contains("i.imgur.com"))
            {
                return source;
            }
            var lastDot = source.LastIndexOf('.');
            string insertSymb;
            switch (thumbType)
            {
                case ImgurThumbType.LargeThumbnail:
                    insertSymb = "l";
                    break;
                case ImgurThumbType.SmallSquare:
                    insertSymb = "s";
                    break;
                default:
                    throw new InvalidEnumArgumentException("ImgurThumbType");
            }
            var res = source.Insert(lastDot, insertSymb);

            if (thumbType == ImgurThumbType.SmallSquare)
                return res;


            var resizedUrl = RemoteReaderPlugin.Current.CreateSignedUrl(res,
                                                                        new ResizeSettings(string.Concat(
                                                                            "height=", 140)));
            return resizedUrl;
        }
    }


    public enum ImgurThumbType
    {
        LargeThumbnail,
        SmallSquare
    }
}