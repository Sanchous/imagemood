var uploader = (function () {

    var out = {};
    var dropbox = $('.dropbox'),
		message = $('.message', dropbox);

    out.setup = function () {
      
      $(document.body).on('click', '.uploaded-image .remove-image', function () {        
            $('.uploaded-image').hide();
            $('.progressHolder').hide();
            $('.uploaded-image img').prop('src', '');
            $('.image-upload #Url').val('');
            $('.image-upload input[type=url]').val('');
            $('.image-upload input[type=file]').val('');
            $('.sources').show();
            
        });

      $(document.body).on('dragenter', dropbox, function () {
            $(this).addClass('over');
        });

        $(document.body).on('dragleave', dropbox, function () {
            $(this).removeClass('over');
        });

        dropbox.filedrop({
            paramname: 'image',
            maxfiles: 1,
            maxfilesize: 5,
            url: 'images/upload',

            uploadFinished: function (i, file, response) {
                $('.image-upload #Url').val(response.imageUrl);
            },

            error: function (err, file) {
                switch (err) {
                    case 'BrowserNotSupported':
                        showMessage('Your browser does not support HTML5 file uploads!');
                        break;
                    case 'TooManyFiles':
                        alert('Only one image at time allowed!');
                        break;
                    case 'FileTooLarge':
                        alert(file.name + ' is too large! Please upload files up to 5mb (configurable).');
                        break;
                    default:
                        break;
                }
            },

            // Called before each upload is started
            beforeEach: function (file) {
                if (!file.type.match(/^image\//)) {
                    alert('Only images are allowed!');

                    // Returning false will cause the
                    // file to be rejected
                    return false;
                }
                $('.dropbox .preview').remove();
            },

            uploadStarted: function (i, file, len) {
                createImage(file);
            },

            progressUpdated: function (i, file, progress) {
                $('.dropbox .progress').css('width', progress + '%');
            }

        });

    };



    function createImage(file) {
        dropbox = $('.dropbox');
        message = $('.message', dropbox);
        var image = $('.uploaded-image img');

        var reader = new FileReader();

        //        image.width = 100;
        //        image.height = 100;

        reader.onload = function (e) {

            // e.target.result holds the DataURL which
            // can be used as a source of the image:

            image.prop('src', e.target.result);

            $('.sources').hide();
            $('.uploaded-image').show();

        };

        // Reading the file as a DataURL. When finished,
        // this will trigger the onload function above:
        reader.readAsDataURL(file);

    }

    function showMessage(msg) {
        message.html(msg);
    }

    return out;
} ());