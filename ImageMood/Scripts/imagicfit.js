﻿var imageFitness = (function () {

    var out = {};
    var minSqueezeRatio = 0.7;
    var appearTimeouts = [];

    var offset = 0;
    var currRowItems = [];

    out.fitImages = function (gallery, newItems, redraw) {

        // clear appearance bugs
        for (var i = 0; i < appearTimeouts.length; i++) {
            clearTimeout(appearTimeouts[i]);
        }

        var items = newItems ? $('li', gallery).not('[data-width]') : $('li', gallery);
        var itemsToSetup = newItems ? items : items.not('[data-width]');
        itemsToSetup.each(function () {
            $(this).attr('data-width', $(this).width());
        });

        if (!newItems) {
            offset = 0;
            currRowItems.length = 0;
        }
        var width = Math.ceil(gallery.width());
        items.each(function (index) {
            var item = $(this);
            if (redraw) {
                item.fadeTo(0, 0);
            }
            var currWidth = parseInt(item.attr('data-width'));
            var remainder = Math.floor(width - offset);
            var rowCompleted = false;

            // Remainder is big enough, resize item to fit it
            if (remainder < currWidth) {
                if (remainder / currWidth > minSqueezeRatio) {
                    item.width(remainder);
                }
                // Remainder is too small to resize last item, resize entire row and set item width to new remainder
                else {
                    var ratio = width / (offset + currWidth);

                    var newRemainder = Math.max(minSqueezeRatio, Math.floor(width - (offset * ratio)));
                    item.width(newRemainder);
                    for (var j = 0, len = currRowItems.length; j < len; j++) {
                        currRowItems[j].width(Math.floor(currRowItems[j].width() * ratio));
                    }
                }
                rowCompleted = true;
            }
            if (rowCompleted) {
                // end up with current row
                // alert(width);
                offset = 0;
                currRowItems.length = 0;
            } else {
                // update item width for case of resizing
                var lastWidth = item.width();
                //        alert("last: " + lastWidth + ", curr: " + currWidth);
                item.width(currWidth);
                // save row state
                offset += currWidth;
                currRowItems.push(item);
            }

            if (redraw) {
                appearTimeouts.push(setTimeout(function () {
                    item.fadeTo(300, 1);
                }, index * 25));
            }
        });
    };


    return out;
} ());

