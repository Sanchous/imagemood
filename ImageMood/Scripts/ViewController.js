﻿

function MoodsViewModel() {

    // Data
    var screenWidth = window.screen.width;

    var self = this;
    self.folders = ['popular', 'new', 'random'];
    self.orders = ['relevant', 'new', 'shuffle'];
    self.randImages = ko.observableArray();

    self.view = ko.observable('moods');
    self.modal = ko.observable('');
    self.chosenFolder = ko.observable();
    self.chosenMoods = ko.observable();
    self.chosenMood = ko.observable();
    self.foundMood = ko.observable('');
    self.chosenMoodName = ko.computed(function () {
        if (self.chosenMood() == null || self.chosenMood == undefined) {
            return "";
        }
        return self.chosenMood().Name;
    });
    self.chosenOrder = ko.observable('');
    self.currentImage = ko.observable();
    self.moodToAdd = ko.observable('');
    self.imagesList = ko.observableArray();
    self.page = 1;
    self.anyImagesLeft = ko.observable(true);
    self.nextImageAvailable = ko.computed(function () {
        if (self.imagesList().length == 0 || self.currentImage() == null)
            return false;
        return self.imagesList()[self.imagesList().length - 1].Id != self.currentImage().Image.Id;
    });
    self.prevImageAvailable = ko.computed(function () {
        if (self.imagesList().length == 0 || self.currentImage() == null)
            return false;
        return self.imagesList()[0].Id != self.currentImage().Image.Id;
    });

    self.wait = ko.observable(false);

    // Models

    function Mood(id, name) {
        return { Id: id, Name: name };
    }

    function Image(image) {
        this.Image = image.Image;
        this.Moods = ko.mapping.fromJS(image.Moods);
    }

    // Behaviours
    self.goToFolder = function (folder) {
        location.hash = folder;
        if (folder == self.folders[2] && location.hash == ('#' + folder)) {
            self.page = 1;
            $.get("/moods/getmoods", { moodOrder: folder }, self.chosenMoods);
        }
    };


    self.exploreMood = function () {
        //
    };
    self.goToMood = function (mood) {
        var plainMood = ko.mapping.toJS(mood);
        location.hash = plainMood.Name + '/' + plainMood.Id + '/' + self.orders[0];
        self.foundMood(' ');
        self.foundMood('');
    };
    self.imagesOfMood = function (order) {
        if (order == self.chosenOrder() && order != self.orders[2])
            return;

        var hash = self.chosenMood().Name + '/' + self.chosenMood().Id + '/' + order;
        if (order == self.orders[2] && location.hash == ('#' + hash)) {
            self.wait(true);
            self.imagesList.removeAll();
            $.get("/moods/findImages/" + self.chosenMood().Id, { imageOrder: order }, function (images) {
                self.onImagesLoaded(images);
            });
        }
        location.hash = hash;
    };

    self.goToImage = function (image) {
        self.moodToAdd('');
        self.wait(true);
        var hash = self.chosenMood().Name + '/' + self.chosenMood().Id + '/' + self.chosenOrder() + '/' + image.Name + '/' + image.Id;
        if (location.hash == ('#' + hash)) {
            $.get("/images/getimage/" + self.chosenMood().Id, { imageId: image.Id }, function (imageData) {
                self.wait(false);
                self.currentImage(new Image(imageData));
            });
        } else {
            location.hash = hash;
        }
    };


    self.nextImage = function (image) {
        var curr = ko.utils.arrayFirst(self.imagesList(), function (item) {
            return image.Image.Id === item.Id;
        });
        var index = self.imagesList.indexOf(curr) + 1;
        self.goToImage(self.imagesList()[index]);
    };

    self.prevImage = function (image) {
        var curr = ko.utils.arrayFirst(self.imagesList(), function (item) {
            return image.Image.Id === item.Id;
        });
        var index = self.imagesList.indexOf(curr) - 1;
        self.goToImage(self.imagesList()[index]);
    };


    self.closeTheater = function (data, event) {

        if (event.target == event.currentTarget) {
            self.modal('');
            self.currentImage(null);
        }

        return true;
    };

    self.moreImages = function () {
        self.wait(true);
        $.get("/moods/findImages/" + self.chosenMood().Id, { page: self.page }, function (images) {
            self.onImagesLoaded(images);
        });
    };

    self.addMood = function (form) {
        if (!$(form).valid()) {
            return false;
        }
        $(form).removeData('validator');
        $(form).removeData('unobtrusiveValidation');
        $.validator.unobtrusive.parse($(form));
        $.ajax(
            {
                url: '/moods/addMood',
                data: JSON.stringify({ mood: new Mood(0, self.moodToAdd()), imageId: self.currentImage().Image.Id }),
                type: 'POST',
                contentType: 'application/json',
                dataType: "json"
            })
            .done(function (imageMood) {
                self.currentImage().Moods.push(ko.mapping.fromJS(imageMood));
                self.moodToAdd('');

                //                var form = $('form');

                //                form.removeData('validator');
                //                form.removeData('unobtrusiveValidation');
                //                $.validator.unobtrusive.parse(form);
            });
        return false;
    };
    self.validateForms = function () {
        var form = $('form');
        form.submit(function () {

            if (!$(this).valid()) {
                return false;
            }
        });
        form.removeData('validator');
        form.removeData('unobtrusiveValidation');
        $.validator.unobtrusive.parse(form);
    };

    self.imageDialog = function () {
        self.modal('addimage');
        self.currentImage(null);
    };

    self.addImage = function (form) {
        $.validator.setDefaults({
            ignore: ""
        });
        if (!$(form).valid()) {
            return false;
        }
        $(form).removeData('validator');
        $(form).removeData('unobtrusiveValidation');
        $.validator.unobtrusive.parse($(form));
        self.wait(true);
        $.ajax(
            {
                url: '/images/add',
                data: $(form).serialize(),
                type: 'POST'
            })
            .done(function (newImageModel) {
                location.hash = newImageModel.MoodName + '/' + newImageModel.MoodId + '/' + self.orders[0]
                    + '/' + newImageModel.Name + '/' + newImageModel.Id;
                self.wait(false);

            })
            .fail(function () {
                alert('failed to add image');
                self.wait(false);
            });
        return false;
    };

    self.vote = function (imageMood) {
        var voted = imageMood.Voted();

        var votedMood = this;
        $.ajax(
            {
                url: '/moods/vote/' + votedMood.Id(),
                data: { plus: !voted },
                type: 'POST'
            })
            .done(function (data) {
                votedMood.Rating(data.Rating);
                votedMood.Voted(!voted);
                self.currentImage().Moods.sort(function (left, right) {
                    return left.Rating() == right.Rating() ? 0 : (left.Rating() > right.Rating() ? -1 : 1);
                });
            });
    };

    self.onImagesLoaded = function (images) {
        var count = images.length;
        if (count <= 0) {
            self.anyImagesLeft(false);
            return;
        }
        var existing = self.imagesList.splice();
        self.imagesList();

        images.forEach(function (item) {
            existing.push(item);
        });
        self.imagesList(existing);
        if (count < 10) {
            self.anyImagesLeft(false);
        } else {
            self.anyImagesLeft(true);
        }

        self.page++;
    };

    // Effects
    var fitTimeout;
    self.fitImages = function (items) {
        var once = false;

        items.forEach(function(item) {
            if (item.nodeType == 1) {
                once = true;
                return;
            }
        });

        if (once) {
            clearTimeout(fitTimeout);
            fitTimeout = setTimeout(function () {
                var img = $('img', $('.images-list')); // Get all img elem
                var count = img.length;
                img.each(function () {
                    var elem = $(this);
                    $("<img/>") // Make in memory copy of image to avoid css issues
                        .attr("src", elem.attr("src"))
                        .load(function () {
                            count--;
                            if (count == 0) {
                                imageFitness.fitImages($('.images-list'), self.imagesList().length == 0, true);
                                self.wait(false);
                            }
                        });
                });

            }, 25);
        }
    };

    var cache2 = {};

    self.afterImageLoad = function (element) {

        self.fadeModal(element);


        var inputToSet = $('#moodAdd input[type=text]').not('.ui-autocomplete-input');
        if (inputToSet.length == 0) {
            return;
        }
        $('#moodAdd input[type=text]').autocomplete({
            minLength: 2,
            source: function (request, response) {
                var term = request.term;
                if (term in cache2) {
                    response(cache2[term]);
                    return;
                }
                $.get("/moods/quicksearch", request, function (data) {
                    cache2[term] = data;
                    response(data);
                });
            },
            select: function (event, ui) {
                self.moodToAdd(ui.item.Name);
            }
        });

    };
    self.fadeModal = function (arr) {
        var once = false;
        arr.forEach(function (item) {
            if (item.nodeType == 1) {
                once = true;
                $(item).fadeTo(0, 0).fadeTo(200, 1);
            }
        });
       
        if (once) {
            $("input[data-autocomplete-source]").each(function () {
                var target = $(this);
                target
                    .bind("keydown", function (event) {
                        if (event.keyCode === $.ui.keyCode.TAB &&
                            $(this).data("ui-autocomplete").menu.active) {
                            event.preventDefault();
                        }
                    })
                    .autocomplete(
                        {
                            source: function (request, response) {
                                var term = request.term.split(/[,;]\s*/).pop();
                                if (!term) {
                                    response([]);
                                    return;
                                }
                                $.getJSON(target.attr('data-autocomplete-source'), { term: term }, response);
                            },
                            focus: function () {
                                // prevent value inserted on focus
                                return false;
                            },
                            select: function (event, ui) {
                                var terms = this.value.split(/,\s*/);
                                terms.pop();
                                terms.push(ui.item.value);
                                terms.push('');
                                this.value = terms.join(', ');
                                return false;
                            }
                        });
            });

            var imageUploadForm = $('.image-upload form');
            if (imageUploadForm.length > 0) {
                $('.image-upload .from-hard').click(function () {
                    $('.image-upload input[type=file]').trigger('click');
                });
                imageUploadForm.ajaxForm({
                    beforeSubmit: function () {
                        $('.progressHolder').show();
                        var percentVal = '0%';
                        $('.progress').width(percentVal);
                        //   percent.html(percentVal);
                    },
                    uploadProgress: function (event, position, total, percentComplete) {


                        var percentVal = percentComplete + '%';
                        $('.progress').width(percentVal);
                        //     percent.html(percentVal);
                    },
                    success: function (response) {
                        var image = $('.uploaded-image img');
                        image.prop('src', response.imageUrl);

                        $('.sources').hide();
                        $('.uploaded-image').show();

                        $('.image-upload #Url').val(response.imageUrl);
                    }
                });
                $('.image-upload form input[name="image"]').change(function () {
                    $(this).closest('form').trigger('submit');
                    $('.image-upload form input[name="imageUrl"]').val('');
                });
                $('.image-upload form input[name="imageUrl"]').bind('paste', function () {
                    var input = $(this);
                    setTimeout(function () {
                        if (input.val().match(/(https?:\/\/.*\.(?:png|jpg))/i) != null) {
                            input.closest('form').trigger('submit');
                        } else {
                            input.val('');
                            alert('Bad image url, try another one');
                        }
                    }, 50);

                });
            }
            self.validateForms();

        }

    };
    self.fadeIn = function (element, index) {

        if (element.nodeType === 1) {
            $(element).fadeTo(0, 0);
            setTimeout(function () {
                $(element).fadeTo(200, 1);
            }, index * 15);
        }
    };
    self.fadeOut = function (element) {
        if (element.nodeType === 1) $(element).slideUp();
    };

    // Routing
    Sammy(function () {

        this.get('#:mood/:id/:order', function () {
            moodRoute(this.params.id, this.params.mood, this.params.order);
        });
        this.get('#:mood/:id/', function () {
            moodRoute(this.params.id, this.params.mood, self.orders[0]);
        });
        this.get('#:mood/:id', function () {
            moodRoute(this.params.id, this.params.mood, self.orders[0]);
        });


        function moodRoute(id, moodName, order) {
            self.view('images');
            self.wait(true);
            var mood = new Mood(id, moodName);
            self.chosenMood(mood);
            self.chosenOrder(order || self.folders[0]);
            self.chosenMoods(null);
            self.currentImage(null);
            self.imagesList.removeAll();
            $.get("/moods/findImages/" + id, { imageOrder: order }, function (images) {
                self.onImagesLoaded(images);
            });
        }

        ;

        this.get('#:folder', function () {
            self.wait(true);
            self.view('moods');
            self.chosenFolder(this.params.folder);
            self.chosenMood(null);
            self.chosenMoods(null);
            self.currentImage(null);
            self.imagesList.removeAll();
            self.page = 1;
            $.get("/moods/getmoods", { moodOrder: this.params.folder }, function (resp) {
                self.chosenMoods(resp);
                self.wait(false);
            });
        });


        this.get('#:mood/:id/:order/:image/:imageId', function () {
            self.view('images');
            self.modal('');
            self.chosenOrder(this.params.order);
            var mood = new Mood(this.params.id, this.params.mood);
            self.chosenMoods(null);
            var moodChanged = self.chosenMood() && (self.chosenMood().Name != mood.Name);
            self.chosenMood(mood);
            self.moodToAdd('');

            if (self.currentImage() != undefined && self.currentImage().Image.Id == this.params.imageId) {
                return;
            }
            $.get("/images/getimage/" + mood.Id, { imageId: this.params.imageId, width: screenWidth }, function (image) {

                self.wait(false);
                self.currentImage(new Image(image));
                if (self.imagesList().length == 0 || moodChanged) {
                    $.get("/moods/findImages/" + mood.Id, { imageOrder: self.chosenOrder() }, function (images) {
                        self.onImagesLoaded(images);
                    });
                }
            });

        });


        this.get('', function () { this.app.runRoute('get', '#' + self.folders[0]); });
    }).run();

};
var viewModel = new MoodsViewModel();
ko.applyBindings(viewModel);