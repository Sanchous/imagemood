﻿// Type: Web.Filters.GzipModule
// Assembly: MightyHelpers, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// Assembly location: D:\Putilin\Dropbox\ImageMood\ImageMood\bin\MightyHelpers.dll

using System;
using System.IO;
using System.IO.Compression;
using System.Web;

namespace ImageMood.Filters
{
    public class GzipModule : IHttpModule
    {
        public void Init(HttpApplication application)
        {
            application.BeginRequest += new EventHandler(this.Application_BeginRequest);
        }

        public void Dispose()
        {
        }

        private void Application_BeginRequest(object source, EventArgs e)
        {
            HttpContext current = HttpContext.Current;
            HttpRequest request = current.Request;
            HttpResponse response = current.Response;
            string str1 = request.Headers["Accept-Encoding"];
            if (string.IsNullOrEmpty(str1))
                return;
            string str2 = str1.ToUpperInvariant();
            if (str2.Contains("GZIP"))
            {
                response.AppendHeader("Content-Encoding", "gzip");
                response.Filter = (Stream)new GZipStream(response.Filter, CompressionMode.Compress);
            }
            else
            {
                if (!str2.Contains("DEFLATE"))
                    return;
                response.AppendHeader("Content-Encoding", "deflate");
                response.Filter = (Stream)new DeflateStream(response.Filter, CompressionMode.Compress);
            }
        }
    }
}
