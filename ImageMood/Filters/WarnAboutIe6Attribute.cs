﻿using System.Web;
using System.Web.Mvc;

namespace ImageMood.Filters
{
    public class WarnAboutIe6Attribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var request = filterContext.HttpContext.Request;
            //this will be true when it's their first visit to the site (will happen again if they clear cookies)
            //if (request.UrlReferrer == null && request.Cookies["browserChecked"] == null)
            //{
                var allow = true;
                //give old IE users a warning the first time
                var browser = request.Browser.Browser.Trim().ToUpperInvariant();
                var version = request.Browser.MajorVersion;

                if (browser == "IE" && version < 9
                    || browser == "OPERA" && version < 7
                    || browser == "SAFARI" && version < 3
                    || browser == "FIREFOX" && version < 2)
                {
                    allow = false;
                }

                if (!allow)
                {

                    filterContext.Result = new RedirectResult("~/Home/HeIsTooOld");
                }            

                filterContext.HttpContext.Response.AppendCookie(new HttpCookie("browserChecked", "true"));
            //}

        }
    }

}