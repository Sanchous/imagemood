﻿using System.Web.Mvc;

namespace ImageMood.Filters
{
    public class JsonForAjaxCallsAttribute : ActionFilterAttribute
    {
         public override void OnActionExecuted(ActionExecutedContext filterContext)
         {
             if (filterContext.RequestContext.HttpContext.Request.IsAjaxRequest() && !(filterContext.Result is JsonResult))
             {
                 filterContext.Result = new JsonResult
                                            {
                                                Data = filterContext.Controller.ViewData.Model,
                                                JsonRequestBehavior = JsonRequestBehavior.AllowGet
                                            };
             }
         }
    }
}