﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Web;

namespace ImageMood.Models
{
    public class UserContext
    {
        private const string UserString = "UserContext";
        public UserContext(int userId)
        {
            if (userId <= 0)
                throw new ArgumentException("Registered user must have a positive UserId");
            UserId = userId;
            VotedIds = new List<int>();
        }

        public UserContext(string guidFromCookie)
        {
            Guid existingGuid;
            UserGuid = !Guid.TryParse(guidFromCookie, out  existingGuid) 
                ? Guid.NewGuid() 
                : existingGuid;
            VotedIds = new List<int>();
            UserName = "Anonymous";
        }

        public int UserId { get; private set; }

        public bool IsRegisteres { get { return UserId > 0; } }

        public Guid UserGuid { get; private set; }

        public string UserName { get; private set; }

        public List<int> VotedIds { get; set; }


        public static UserContext GetContext()
        {
            var context = (UserContext)HttpContext.Current.Session[UserString];
            if (context == null)
            {
                var cookie = HttpContext.Current.Request.Cookies[UserString];
                bool needSet = cookie == null;
                context = new UserContext(needSet ? "" : cookie.Value);
                if (needSet)
                {
                    var newCookie = new HttpCookie(UserString, context.UserGuid.ToString())
                                        {
                                            Expires = DateTime.Now.AddYears(100),

                                        };
                    
                    HttpContext.Current.Response.Cookies.Add(newCookie);
                }
                HttpContext.Current.Session.Add(UserString, context);
            }
            return context;
        }
        
    }
}