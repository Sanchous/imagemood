﻿using System;

namespace ImageMood.Models
{
    public class NotFoundException : Exception
    {
        public NotFoundException()
        {
            
        }

        public NotFoundException(string message):base(message)
        {
            
        }
    }
}