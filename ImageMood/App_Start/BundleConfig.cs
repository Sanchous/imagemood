﻿using System.Web.Optimization;

namespace ImageMood.App_Start
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
                        "~/Scripts/jquery-ui-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.unobtrusive*",
                        "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/knockout").Include(
                        "~/Scripts/knockout-*",
                        "~/Scripts/imagicfit.js",
                        "~/Scripts/knockout.mapping-latest*",
                        "~/Scripts/sammy-*",
                        "~/Scripts/ViewController.js"));

            bundles.Add(new ScriptBundle("~/bundles/dropbox").Include(
                "~/Scripts/filedrop/jquery.filedrop.js",
                "~/Scripts/filedrop/script.js",
                "~/Scripts/jquery.form.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                "~/Content/main-site.css",
                "~/Content/font-awesome.css"));

            bundles.Add(new StyleBundle("~/Content/themes/custom/css").Include(
                        "~/Content/themes/jquery-ui-1.10.0.custom.css"));

         //   BundleTable.EnableOptimizations = true;

        }
    }
}