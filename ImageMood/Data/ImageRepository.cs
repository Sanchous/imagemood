﻿using System.Data.Entity;
using ImageMood.Entities;
using MightyHelpers.Repository;

namespace ImageMood.Data
{
    public class ImageRepository : EfRepository<Image>, IImageRepository
    {
        public ImageRepository(DbContext dataContext) : base(dataContext)
        {
        }
    }
}