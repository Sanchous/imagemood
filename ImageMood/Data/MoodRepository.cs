﻿using System.Data.Entity;
using ImageMood.Entities;
using MightyHelpers.Repository;

namespace ImageMood.Data
{
    public class MoodRepository : EfRepository<Mood>, IMoodRepository
    {
        public MoodRepository(DbContext dataContext) : base(dataContext)
        {
        }
    }
}