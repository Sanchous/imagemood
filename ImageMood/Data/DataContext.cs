﻿using System.Data.Entity;
using ImageMood.Entities;

namespace ImageMood.Data
{
    public class DataContext : DbContext
    {
        public DbSet<Image> Images { get; set; }

        public DbSet<Mood> Moods { get; set; }

        public DataContext()
            : base("DefaultConnection")
        {

        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Image>()
                        .HasMany(x => x.ImageMoods)
                        .WithRequired(x => x.Image)
                        .WillCascadeOnDelete(true)
                ;
            modelBuilder.Entity<Mood>()
                        .HasMany(x => x.MoodToImages)
                        .WithRequired(x => x.Mood)
                        .WillCascadeOnDelete(true)
                ;

            

            base.OnModelCreating(modelBuilder);
        }
    }
}