﻿using ImageMood.Entities;
using MightyHelpers.Repository;

namespace ImageMood.Data
{
    public interface IMoodRepository : IRepository<Mood>
    {
         
    }
}