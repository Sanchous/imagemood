using System;
using System.Linq;
using System.Linq.Expressions;
using ImageMood.Entities;
using ImageMood.Models;

namespace ImageMood.ViewModels
{
    public class ImageMoodViewModel
    {
        public int Id { get; set; }
        public MoodViewModel Mood { get; set; }
        public int Rating { get; set; }
        public bool Voted { get; set; }

        public static Expression<Func<MoodToImage, ImageMoodViewModel>> Selector(Guid userGuid)
        {
            return im => new ImageMoodViewModel
                {
                    Id = im.Id,
                    Mood = new MoodViewModel
                        {
                            Id = im.MoodId,
                            Name = im.Mood.Name
                        },
                    Rating = im.Rating,
                    Voted =
                        im.Votes.Any(
                            a => a.AnonymousGuid == userGuid)
                };
        }

    }
}