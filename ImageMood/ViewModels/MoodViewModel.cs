namespace ImageMood.ViewModels
{
    public class MoodViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}