﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using ImageMood.Entities;
using ImageMood.Models;

namespace ImageMood.ViewModels
{
    public class ImageViewModel
    {
        //public int Id { get; set; }
        //public string Name { get; set; }
        //public string Description { get; set; }
        //public string Url { get; set; }
        public Image Image { get; set; }
        public IEnumerable<ImageMoodViewModel> Moods { get; set; }

        public static Expression<Func<Image, ImageViewModel>> Selector(Guid userGuid)
        {
            return s => new ImageViewModel
                {
                    Image = s,
                    Moods = s.ImageMoods
                             .AsQueryable()
                             .Select(im => new ImageMoodViewModel
                             {
                               Id = im.Id,
                               Mood = new MoodViewModel
                               {
                                 Id = im.MoodId,
                                 Name = im.Mood.Name
                               },
                               Rating = im.Rating,
                               Voted =
                        im.Votes.Any(
                            a => a.AnonymousGuid == userGuid)
                             })
                             .OrderByDescending(o => o)
                };
        }
    }
}