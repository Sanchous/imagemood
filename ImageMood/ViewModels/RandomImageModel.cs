﻿using System;
using System.Linq;
using System.Linq.Expressions;
using ImageMood.Entities;

namespace ImageMood.ViewModels
{
    public class RandomImageModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public MoodViewModel Mood { get; set; }

        public static readonly Expression<Func<Image, RandomImageModel>> Selector = s => new RandomImageModel
            {
                Id = s.Id,
                Name = s.Name,
                Mood =
                    s.ImageMoods.OrderBy(o => Guid.NewGuid())
                     .Select(mi => mi.Mood)
                     .Select(m => new MoodViewModel
                         {
                             Id = m.Id,
                             Name = m.Name
                         }).FirstOrDefault()
            };

    }
}