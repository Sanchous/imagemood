﻿namespace ImageMood.Enums
{
    public enum MoodOrder
    {
        Rating,
        Popular,
        New,
        Random
    }
}