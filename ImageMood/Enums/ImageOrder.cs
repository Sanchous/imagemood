﻿namespace ImageMood.Enums
{
    public enum ImageOrder
    {
        Shuffle,
        Relevant,
        New
    }
}